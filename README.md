# ClassRot

Code based on Eriguchi, Muller method to compute equilibrium configuration of rotating polytropic stars in Newtonian Gravity

It implements a two parameter differential rotation profile. See the citation paper for more information.

CITATION: if you use the code, please cite:

- Cipolletta, F., Cherubini, C., Filippi, S., Rueda, J., & Ruffini, R. (2017). Equilibrium Configurations of Classical Polytropic Stars with a Multi-Parametric Differential Rotation Law: A Numerical Analysis. Communications in Computational Physics, 22(3), 863-888. doi:10.4208/cicp.OA-2017-0007

